# Arduino Mini Pro Firmware

Firmware for the SparkFun [Arduino Pro Mini](https://www.sparkfun.com/products/11114) board.

It contains a 8-bit ATmega328P micro-controller (32kB Flash, 2kB RAM) running at 3.3V with a 8 MHz external crystal oscillator.

For more details about the hardware, check the following documents:

 * Arduino Mini Pro [datasheet](doc/ProMini8MHzv2.pdf), [schematic](doc/Arduino-Pro-Mini-v14.pdf)
 * ATmega328P [datasheet](doc/Atmel-7810-Automotive-Microcontrollers-ATmega328P_Datasheet.pdf)
